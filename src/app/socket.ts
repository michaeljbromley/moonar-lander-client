import {Subject, Observable} from 'rxjs';
import {CommandSet} from './models';

enum ReadyState {
    Connection,
    Open,
    Closing,
    Closed
}

export class Socket {

    message$ = new Subject<any>();
    error$ = new Subject<any>();

    private ws: WebSocket;
    private isConnected: boolean = false;

    constructor(private url: string, private playerName: string) {}

    connect(): Observable<boolean> {
        if (this.ws && this.ws.readyState === ReadyState.Open) {
            return Observable.of(true);
        }
        this.ws = new WebSocket(this.url);
        return new Observable((observer) => {

            this.ws.onopen = () => {
                this.isConnected = true;
                console.log('onopen');
                this.send({ player: this.playerName });
                console.log(`Registering as ${this.playerName}`);
                observer.next(true);
                observer.complete();
            };

            this.ws.onmessage = message => {
                const data = JSON.parse(message.data);
                if (data.game === 'start') {
                    console.log('Starting the game! 🚀🚀🚀🚀🚀🚀');
                }
                this.message$.next(data);
            };

            this.ws.onclose = ev => {
                console.log('CLOSED', ev);
            };

            this.ws.onerror = err => {
                console.error(err);
            };
        });
    }

    send(payload: CommandSet | { player: string; }): void {
        this.ws.send(JSON.stringify(payload));
    }

}