import {TerrainMap, CommandSet, Status, GameOver} from './models';
export function $(selector: string): HTMLElement {
    return document.querySelector(selector) as HTMLElement;
}

export function isTerrainMap(obj: any): obj is TerrainMap {
    return obj.hasOwnProperty('terrain') && obj.hasOwnProperty('flagPosition');
}

export function isCommandSet(obj: any): obj is CommandSet {
    return obj.hasOwnProperty('commands');
}

export function isStatus(obj: any): obj is Status {
    return obj.hasOwnProperty('tick') && obj.hasOwnProperty('position');
}

export function isGameOver(obj: any): obj is GameOver {
    return obj.hasOwnProperty('game') && obj.game === 'over';
}
