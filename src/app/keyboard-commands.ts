import {Observable} from 'rxjs';
import {CommandSet, Command} from './models';

const UP = 38;
const DOWN = 40;
const LEFT = 37;
const RIGHT = 39;

const keydown$ = Observable.fromEvent<KeyboardEvent>(document, 'keydown')
    .map(event => event.keyCode)
    .filter(keyCode => -1 < [UP, DOWN, LEFT, RIGHT].indexOf(keyCode))
    .map(key => {
        const commandMap = {
            [LEFT]: { rotation: "ccw" },
            [RIGHT]: { rotation: "cw" },
            [UP]: { engine: "full" },
            [DOWN]: { engine: "half" }
        };
        return commandMap[key] as Command;
    });

const keyup$: Observable<Command> = Observable.fromEvent<KeyboardEvent>(document, 'keyup')
    .map(event => event.keyCode)
    .filter(keyCode => -1 < [UP, DOWN, LEFT, RIGHT].indexOf(keyCode))
    .map(key => {
        const commandMap = {
            [LEFT]: { rotation: "off" },
            [RIGHT]: { rotation: "off" },
            [UP]: { engine: "off" },
            [DOWN]: { engine: "off" }
        };
        return commandMap[key] as Command;
    });

export const keyboardCommands$: Observable<CommandSet> = Observable.merge(keydown$, keyup$)
    .map(command => ({ commands: [command] }));
