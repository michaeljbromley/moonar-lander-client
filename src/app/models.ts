export type EngineState = 'off' | 'full' | 'half';
export type RotationState = 'off' | 'cw' | 'ccw';

export interface Command {
    engine?: EngineState;
    rotation?: RotationState;
    tick?: number;
}

export interface CommandSet {
    commands: Command[];
}

export interface Point {
    x: number;
    y: number;
}

export interface Vec2 {
    x: number;
    y: number;
}

export interface TerrainMap {
    seed: string;
    terrain: Point[];
    flagPosition: Point;
    startPosition: Point;
    startVelocity: Point;
    startAngle: number;
}

export interface Status {
    player: string;
    color: string;
    position: Point;
    velocity: Vec2;
    angle: number;
    tick: number;
    rotation: RotationState;
    rotationSpeed: number;
    engine: EngineState;
    fuel: number;
    crashed: boolean;
    landed: boolean;
    touchdown: boolean;
}

export interface GameOver {
    game: 'over';
}
