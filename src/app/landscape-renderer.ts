import {TerrainMap, Point, Status} from './models';
import {Observable} from 'rxjs';
import {trajectory} from './trajectory';
import {calculateOptimumLandingSite, calculateXIntersection} from './computer';

export class LandscapeRenderer {

    private ctx: CanvasRenderingContext2D;
    private terrainMap: TerrainMap;
    private status: Status;
    private landingSite: Point;
    private trajectory: Point[] = [];

    constructor(private canvasElement: HTMLCanvasElement) {
        this.ctx = canvasElement.getContext('2d') as CanvasRenderingContext2D;

        Observable.fromEvent<Event>(window, 'resize')
            .startWith({})
            .subscribe(() => {
               canvasElement.width = window.innerWidth;
               canvasElement.height = window.innerWidth / 4;
            });
    }

    loadTerrain(terrainMap: TerrainMap): void {
        this.terrainMap = terrainMap;
        this.landingSite = calculateOptimumLandingSite(terrainMap);
        requestAnimationFrame(() => this.render());
    }

    updateLanderPosition(status: Status): void {
        this.status = status;
        this.trajectory = trajectory(status);
    }

    private render(): void {
        const scale = this.getScale();
        this.ctx.lineWidth = 1;
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.renderGrid(scale);
        this.renderTerrain(scale);
        this.renderFlag(scale);
        this.renderLandingSite(scale);
        this.renderLander(scale);
        this.renderTrajectory(scale);
        this.renderIntersection(scale);

        requestAnimationFrame(() => this.render());
    }

    private renderGrid(scale: number): void {
        this.ctx.lineWidth = 1;
        this.ctx.setLineDash([]);
        this.ctx.strokeStyle = '#354335';
        for(let i = 1; i < 10; i++) {
            let x = i * 1000 * scale;
            this.ctx.beginPath();
            this.ctx.moveTo(x, 0);
            this.ctx.lineTo(x, this.ctx.canvas.height);
            this.ctx.stroke();

            this.ctx.beginPath();
            this.ctx.moveTo(0, this.ctx.canvas.height - i * 1000 * scale);
            this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height - i * 1000 * scale);
            this.ctx.stroke();
        }
    }

    private renderTerrain(scale: number): void {
        this.terrainMap.terrain.forEach((rawPoint, index) => {
            const point = this.normalize(rawPoint, scale);
            if (index === 0) {
                this.ctx.moveTo(point.x, point.y);
                this.ctx.beginPath();
            } else {
                this.ctx.lineTo(point.x, point.y);
            }
        });
        this.ctx.setLineDash([]);
        this.ctx.strokeStyle = '#aaa';
        this.ctx.stroke();
    }

    private renderFlag(scale: number): void {
        const flag = this.normalize(this.terrainMap.flagPosition, scale);
        this.ctx.fillStyle = '#692424';
        this.ctx.beginPath();
        this.ctx.arc(flag.x, flag.y, 4, 0, 2 * Math.PI);
        this.ctx.fill();
    }

    private renderLandingSite(scale: number): void {
        const flag = this.normalize(this.landingSite, scale);
        this.ctx.fillStyle = '#ff260c';
        this.ctx.beginPath();
        this.ctx.arc(flag.x, flag.y, 3, 0, 2 * Math.PI);
        this.ctx.fill();
    }

    private renderLander(scale: number): void {
        if (this.status) {
            const lander = this.normalize(this.status.position, scale);
            this.ctx.fillStyle = '#00510e';
            this.ctx.strokeStyle = '#00ce20';
            this.ctx.save();
            this.ctx.beginPath();
            this.ctx.translate(lander.x, lander.y);
            this.ctx.rotate(this.status.angle * -1);
            this.ctx.moveTo(0, - 6);
            this.ctx.lineTo(- 5, 6);
            this.ctx.lineTo(5, 6);
            this.ctx.closePath();
            this.ctx.fill();
            this.ctx.stroke();
            this.ctx.restore();
        }
    }

    private renderTrajectory(scale: number): void {
        this.ctx.strokeStyle = '#00cb48';
        this.ctx.lineWidth = 1;
        this.ctx.setLineDash([4, 1]);
        this.trajectory.forEach((rawPoint, i) => {
            const point = this.normalize(rawPoint, scale);
            if (i === 0) {
                this.ctx.beginPath();
                this.ctx.moveTo(point.x, point.y);
            }
            if (i % 10 === 0) {
                // this.ctx.fillStyle = '#009317';
                // this.ctx.arc(point.x, point.y, 0.5, 0, Math.PI * 2);
                // this.ctx.fill();
                this.ctx.lineTo(point.x, point.y);
            }
            if (i === this.trajectory.length - 1) {
                this.ctx.stroke();
            }
        });
    }

    private renderIntersection(scale: number): void {
        if (this.status) {
            const intersection = calculateXIntersection(this.status);
            const point = this.normalize({ x: intersection, y: 0}, scale);
            this.ctx.strokeStyle = '#00cb48';
            this.ctx.lineWidth = 1;
            this.ctx.setLineDash([4, 1]);
            this.ctx.beginPath();
            this.ctx.moveTo(point.x, point.y);
            this.ctx.lineTo(point.x, point.y - 20);
            this.ctx.stroke();
        }
    }

    private getScale(): number {
        const terrain = this.terrainMap.terrain;
        const terrainWidth = terrain[terrain.length - 1].x;
        const canvasWidth = this.ctx.canvas.width;
        return canvasWidth / terrainWidth;
    }

    /**
     * Normalize a point received from the game by inverting the Y value and aligning it with the
     * current canvas scale.
     */
    private normalize(point: Point, scale: number): Point {
        return this.invertY(this.scale(point, scale), this.ctx.canvas.height);
    }

    private scale(point: Point, scale: number): Point {
        return { x: point.x * scale, y: point.y * scale };
    }

    private invertY(point: Point, height: number): Point {
        return { x: point.x, y: height - point.y };
    }
}
