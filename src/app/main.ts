import {Observable} from 'rxjs';
import {Socket} from './socket';
import {$, isTerrainMap, isCommandSet, isStatus, isGameOver} from './utils';
import {keyboardCommands$} from './keyboard-commands';
import {LandscapeRenderer} from './landscape-renderer';
import {Computer} from './computer';

declare const require: any;
require('../styles/main.scss');

const playerName = "jean_luc_skywalker";

const startButton = $('#start') as HTMLButtonElement;
const landscapeCanvas = $('#landscape') as HTMLCanvasElement;
const socket = new Socket('ws://10.3.16.43:4711', playerName);
const landscapeRenderer = new LandscapeRenderer(landscapeCanvas);
const computer = new Computer();

const startClick$ = Observable.fromEvent(startButton, 'click');


startClick$
    .do(() => startButton.disabled = true)
    .switchMap(() => socket.connect())
    .switchMap(() => socket.message$)
    .merge(keyboardCommands$)
    .subscribe(val => {
            // console.log(val);
            if (isCommandSet(val)) {
                socket.send(val);
            }
            if (isTerrainMap(val)) {
                console.info('LEVEL', val);
                landscapeRenderer.loadTerrain(val);
                computer.loadTerrain(val)
            }
            if (isStatus(val)) {
                if (val.landed) {
                    console.info('LANDED 👌👌👌', status);
                }
                if (val.crashed) {
                    console.info('CRASHED 😢😢😢', status);
                }
                landscapeRenderer.updateLanderPosition(val);
                const commands = computer.processStatus(val);
                if (0 < commands.commands.length) {
                    console.log('%c sending commands', 'color: #336', commands);
                    socket.send(commands);
                }

                if (val.tick % 100 === 0) {
                    console.log(`%c TICK: ${val.tick}`, 'color: #666');
                }
            }
            if (isGameOver(val)) {
                // reset the game
                console.info('GAME OVER', val);
                startButton.disabled = false;
                computer.reset();
            }
        },
        err => console.error(err)
    );
