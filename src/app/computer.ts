import {Command, CommandSet, Point, RotationState, Status, TerrainMap} from './models';

// Physical constants which power the physics of the game.
// Note: CW rotation is negative, so due east === -PI/2.
const ROTATION_ACCELERATION = 0.0015;
const ROTATION_DAMPING = 0.0001;
const FULL_ROTATION = 2 * Math.PI;
const GRAVITY = 0.01;
const FULL_THRUST = 0.035;
const HALF_THRUST = FULL_THRUST * 0.6;

enum Stage {
    Init,
    Stabilise,
    RotateTowardsTarget,
    AccelerateTowardsTarget,
    RotateForDeceleration,
    DecelerateToTarget,
    RotateToLandingAngle,
    DecelerateToLand,
    FineAdjustmentsToLand,
    Touchdown
}

interface CalculationResult { tick: number; commands: Command[]; }
interface StageComputationResult { goToNextStage: boolean; commands?: Command[] }

/**
 * The automatic navigation computer which will direct the smooth landing of the lander.
 */
export class Computer {

    terrainMap: TerrainMap;
    target: Point;
    stage: Stage = 0;
    initialAngle: number;
    initialDistanceFromTarget: number;
    decelerationAngle: number;
    doStageComputation: (status: Status) => StageComputationResult = (_) => ({ goToNextStage: true });

    loadTerrain(terrainMap: TerrainMap): void {
        this.terrainMap = terrainMap;
        this.target = calculateOptimumLandingSite(terrainMap);
        this.decelerationAngle = calculateDecelerationAngle();
    }

    processStatus(status: Status): CommandSet {
        if (!this.initialDistanceFromTarget) {
            this.initialDistanceFromTarget = Math.abs(this.target.x - status.position.x);
        }
        let result: CalculationResult;
        let nextActionableTick: number;
        let commands: Command[] = [];
        const computationResult = this.doStageComputation(status);
        if (computationResult.commands) {
            commands = commands.concat(computationResult.commands);
        }
        if (computationResult.goToNextStage) {
            this.stage ++;
            console.log(`%c Current stage: ${Stage[this.stage]}`, 'color: #393; background: #dfd;');
            switch (this.stage) {
                case Stage.Stabilise:
                    if (initialDescentTooFast(status)) {
                        this.doStageComputation = status => {
                            return stabiliseInitialPosition(status, this.target);
                        }
                    } else {
                        this.doStageComputation = _ => ({ goToNextStage: true });
                    }
                    break;
                case Stage.RotateTowardsTarget:
                    this.initialAngle = calculateInitialAngle(status.position, this.target);
                    result = rotateTo(status, this.correctInitialAngle(status.position));
                    this.doStageComputation = status => {
                        const goToNextStage = (result.tick < status.tick);
                        return { goToNextStage };
                    };
                    commands = commands.concat(result.commands);
                    break;
                case Stage.AccelerateTowardsTarget:
                    this.doStageComputation = status => {
                        return accelerateToTarget(status, this.target, this.initialDistanceFromTarget / 2);
                    };
                    commands.push({ engine: 'full' });
                    break;
                case Stage.RotateForDeceleration:
                    result = rotateTo(status, this.correctDecelerationAngle(status.position));
                    this.doStageComputation = status => {
                        const goToNextStage = (result.tick < status.tick);
                        return { goToNextStage };
                    };
                    commands = commands.concat(result.commands);
                    break;
                case Stage.DecelerateToTarget:
                    result = decelerateToTarget(status, this.target);
                    this.doStageComputation = status => {
                        const goToNextStage = (result.tick < status.tick);
                        return { goToNextStage };
                    };
                    commands = commands.concat(result.commands);
                    break;
                case Stage.RotateToLandingAngle:
                    result = rotateTo(status, 0);
                    nextActionableTick = result.tick;
                    this.doStageComputation = status => {
                        let goToNextStage = false;
                        let commands: Command[] = [];
                        if (nextActionableTick < status.tick) {
                            if (Math.abs(status.angle) < 0.01) {
                                console.log(`angle is okay! ${status.angle}`);
                                goToNextStage = true;
                            } else {
                                console.log(`angle is ${status.angle}, correcting again`);
                                const result = rotateTo(status, 0);
                                nextActionableTick = result.tick;
                                commands = result.commands;
                            }
                        }
                        return { goToNextStage, commands };
                    };
                    commands = commands.concat(result.commands);
                    break;
                case Stage.DecelerateToLand:
                    console.warn(`DECELRATING, current angle = ${status.angle}`);
                    this.doStageComputation = status => {
                        return decelerateToLand(status, this.terrainMap);
                    };
                    commands.push({ engine: 'off' });
                    break;
                case Stage.FineAdjustmentsToLand:
                    nextActionableTick = commands[1].tick!;
                    let balance = 0;
                    this.doStageComputation = status => {
                        if (nextActionableTick <= status.tick) {
                            const result = makeFinalAdjustments(status, this.terrainMap, this.target, balance);
                            balance = result.balance;
                            return result;
                        } else {
                            return {
                                commands: [],
                                goToNextStage: false
                            };
                        }
                    };
                    break;
                case Stage.Touchdown:
                    nextActionableTick = status.tick;
                    this.doStageComputation = status => {
                        if (nextActionableTick <= status.tick) {
                            const res = matchAngleToTerrain(status, this.terrainMap);
                            nextActionableTick = res.tick;
                            return res;
                        } else {
                            return {
                                commands: [],
                                goToNextStage: false
                            };
                        }
                    };
                    commands = commands.concat({ engine: 'off' });
                    break;
                default:
            }
        }

        return { commands };
    }

    reset(): void {
        this.stage = Stage.Init;
        delete this.initialDistanceFromTarget;
    }

    /**
     * Invert the initialAngle depending on which side of the target we start from.
     */
    private correctInitialAngle(position: Point): number {
        return position.x < this.target.x ? this.initialAngle * -1 : this.initialAngle;
    }

    private correctDecelerationAngle(position: Point): number {
        return position.x < this.target.x ? this.decelerationAngle : this.decelerationAngle * -1;
    }
}

/**
 * Calculates the closest safest landing point to the flag. Returns the point itself, plus the angle of
 * incline between the two surrounding terrain points.
 */
export function calculateOptimumLandingSite(terrainMap: TerrainMap): Point {
    let bestMatch: {
        point: Point,
        proximity: number
    } | undefined;

    // maximum variation in y value allowable for a point to be "flat enough"
    const maxPitch = 10;

    // first we check to see if it would be possible to land on the flag itself
    const separation = terrainMap.terrain[1].x;
    const flagIndex = Math.round(terrainMap.flagPosition.x / separation);
    const flagPrev = terrainMap.terrain[flagIndex - 1];
    const flag = terrainMap.terrain[flagIndex];
    const flagNext = terrainMap.terrain[flagIndex + 1];
    const tolerance = 0.349066; // 20 degrees in rad
    const flagAngle = findAngle(flagPrev, flag, flagNext);
    const heightVariation = Math.max(flagPrev.y, flag.y, flagNext.y) - Math.min(flagPrev.y, flag.y, flagNext.y);
    const isLevelEnough = heightVariation < 40;
    if (isLevelEnough&& (Math.PI - tolerance * 3) <= flagAngle && flagAngle <= (Math.PI + tolerance)) {
        // we can land right on the flag!
        return flag;
    }

    for(let i = 1; i < terrainMap.terrain.length - 2; i++) {
        const point = terrainMap.terrain[i];
        const proximity = Math.abs(point.x - terrainMap.flagPosition.x);

        //const prev = terrainMap.terrain[i - 1];
        const next = terrainMap.terrain[i + 1];
        const min = Math.min(point.y, next.y);
        const max = Math.max(point.y, next.y);
        const flatEnough = max - min <= maxPitch;

        if (!bestMatch || (flatEnough && proximity < bestMatch.proximity)) {
            const midPoint: Point = {
                x: (point.x + next.x) / 2,
                y: Math.max(point.y, next.y)
            };
            bestMatch =  { point: midPoint, proximity };
        }
    }
    return bestMatch!.point;
}

/**
 * Given three points, calculate the angle in radians between them. P1 is the centre point.
 */
function findAngle(p0: Point, p1: Point, p2: Point): number {
    const b = Math.pow(p1.x-p0.x,2) + Math.pow(p1.y-p0.y,2);
    const a = Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2);
    const c = Math.pow(p2.x-p0.x,2) + Math.pow(p2.y-p0.y,2);
    return Math.acos( (a+b-c) / Math.sqrt(4*a*b) );
}

function initialDescentTooFast(status: Status): boolean {
    return status.velocity.y < -1;
}

/**
 * An initial crash can occur if a) lander starts off rapidly falling and /or b) starting pos is so low
 * that it is likely that the trajectory will intersect mountainous terrain.
 */
function stabiliseInitialPosition(status: Status, target: Point): StageComputationResult {
    let commands: Command[] = [];
    let goToNextStage = false;
    const minHeight = target.y + 200;
    if (0.05 < Math.abs(status.angle) && status.rotation === 'off') {
        commands = commands.concat(rotateTo(status, 0).commands);
    }
    if (status.velocity.y < -0.1 || status.position.y < minHeight) {
        if (Math.abs(status.angle) < 0.1) {
            if (status.engine === 'off') {
                commands.push({engine: 'full'});
            }
        }
    } else {
        commands.push({ engine: 'off' });
        goToNextStage = true;
    }
    return { commands, goToNextStage };
}

/**
 * The closer we start to the target, the more vertical the trajectory should be
 */
function calculateInitialAngle(startPos: Point, target: Point): number {
    const horizontalDistance = Math.abs(startPos.x - target.x);
    // const verticalHeight = horizontalDistance + (10000 / horizontalDistance) ** 2;
    let angleCorrection = 0;
    if (horizontalDistance < 1000) {
        angleCorrection = 1000;
    } else if (horizontalDistance < 2000) {
        angleCorrection = 500;
    } else if (4000 < horizontalDistance) {
        angleCorrection = -horizontalDistance / 5;
    }
    angleCorrection += (5000 / horizontalDistance) ** 2;
    const verticalHeight = Math.max(horizontalDistance - startPos.y / 2 + angleCorrection, horizontalDistance / 10);
    return Math.PI / 2 - Math.min(Math.atan(verticalHeight / horizontalDistance), Math.PI / 2.5);
}

function calculateDecelerationAngle(): number {
    return Math.PI / 2 - Math.asin(GRAVITY / FULL_THRUST / 2);
    //return Math.PI / 2;
}

/**
 *  To figure out how far we want to initially accelerate for, we do
 *  1. Calculate the current trajectory and find out where it crosses the x axis
 *  2. If the intersection is not at or beyond a specified threshold beyond the target, keep thrusting
 *  3. Repeat 1 and 2 until the intersection is at or beyond the threshold
 *
 */
function accelerateToTarget(status: Status, target: Point, overshoot: number): StageComputationResult {
    const movingRight = status.position.x < target.x;
    // when the target is very close, we need to overshoot more to give enough
    // time to perform all stages
    const correctedOvershoot = overshoot < 500 ? overshoot * 2 : overshoot;
    const threshold = movingRight ? target.x + correctedOvershoot : target.x - correctedOvershoot;

    const intersection = calculateXIntersection(status);
    const goToNextStage = movingRight ? threshold <= intersection : intersection <= threshold;
    const commands: Command[] | undefined = goToNextStage ? [ { engine: 'off' } ] : undefined;
    return {
        goToNextStage,
        commands
    };
}

/**
 * Given the current velocity of the lander, return the x coordinate at which it would
 * intersect the x axis if it follows its current trajectory.
 */
export function calculateXIntersection(status: Status): number {
    const vY = status.velocity.y;
    const height = status.position.y;
    const ticksToFall = calculateTimeToReachDistance(-vY, GRAVITY, height);
    return status.position.x + status.velocity.x * ticksToFall;
}

function decelerateToTarget(status: Status, target: Point): CalculationResult {
    const initTick = status.tick;
    const position = status.position;
    const velocity = status.velocity;

    const ticksToStop = calculateTimeToStopX(status);

    const a = Math.abs((Math.sin(status.angle) * FULL_THRUST));
    const stoppingDistance = calculateStoppingDistance(velocity.x, a);

    const ticksToReachTarget = ( (Math.abs(target.x - position.x) - stoppingDistance) / Math.abs(velocity.x) ) + ticksToStop;
    const t1 = initTick + round(ticksToReachTarget - ticksToStop);
    const t2 = t1 + round(ticksToStop);
    return {
        tick: t2 + 1,
        commands: [
            { engine: 'full', tick: t1 },
            { engine: 'off', tick: t2 }
        ]
    }
}

/**
 * Bring the lander down to the target site
 */
function decelerateToLand(status: Status, map: TerrainMap): StageComputationResult {
    const heightAboveGround = status.position.y - getElevationAt(status.position.x, map);
    const a = FULL_THRUST - GRAVITY;
    const ticksToRest = Math.round(Math.abs(status.velocity.y / a));
    const stoppingDistance = calculateStoppingDistance(status.velocity.y, a);
    const padding = 40; // safety factor
    let commands: Command[] | undefined;
    let goToNextStage = false;
    if (heightAboveGround < stoppingDistance + padding) {
        commands = [
            { engine: 'full' },
            { engine: 'off', tick: status.tick + ticksToRest }
        ];
        goToNextStage = true;
    }
    return {
        goToNextStage,
        commands
    };
}

function makeFinalAdjustments(status: Status, map: TerrainMap, target: Point, balance: number): StageComputationResult & { balance: number } {
    const [prev, next] = getSurroundingPoints(status.position, map.terrain);
    const engineCutoffPoint = (prev.y + next.y) / 2 + 10;
    const goToNextStage = status.position.y < engineCutoffPoint;
    let commands: Command[] = [];
    let newBalance = balance;

    const correctionAngle = 0.125;
    const ticksToRotate = correctionAngle / ROTATION_ACCELERATION * 2;
    const minProximity = 12;

    function tooFarLeft(pos: Point): boolean {
        return minProximity < pos.x - target.x;
    }

    function tooFarRight(pos: Point): boolean {
        return minProximity < target.x - pos.x;
    }

    function closeEnough(pos: Point): boolean {
        return Math.abs(pos.x - target.x) < minProximity;
    }

    if (status.rotation === 'off') {
        const projectedPos: Point = {
            x: status.position.x + status.velocity.x * ticksToRotate,
            y: status.position.y
        };
        if (newBalance === -1) {
            if (closeEnough(projectedPos)) {
                commands = commands.concat(rotateTo(status, correctionAngle).commands);
                newBalance = 0;
            }
        } else if (newBalance === 1) {
            if (closeEnough(projectedPos)) {
                commands = commands.concat(rotateTo(status, -correctionAngle).commands);
                newBalance = 0;
            }
        } else if (newBalance === 0) {
            if (tooFarLeft(status.position) && balance < 1) {
                commands = commands.concat(rotateTo(status, correctionAngle).commands);
                newBalance = 1;
            } else if (tooFarRight(status.position) && balance > -1) {
                commands = commands.concat(rotateTo(status, -correctionAngle).commands);
                newBalance = -1;
            } else {
                commands = commands.concat(rotateTo(status, 0).commands);
            }
        }
    }

    // A crash occurs when velocity is less than -1
    if (status.velocity.y < -0.2) {
        commands.push({ engine: 'half' });
    } else {
        commands.push({ engine: 'off' });
    }
    return {
        goToNextStage,
        commands,
        balance: newBalance
    };
}

function matchAngleToTerrain(status: Status, map: TerrainMap): StageComputationResult & { tick: number }{
    const [prev, next] = getSurroundingPoints(status.position, map.terrain);
    const maxLandingAngle = Math.PI / 4;
    const terrainAngle = Math.atan((next.y - prev.y) / (next.x - prev.x));
    const fullRotations = Math.floor(Math.abs(status.angle / FULL_ROTATION)) * Math.sign(status.angle);
    let correctedAngle: number;
    if (maxLandingAngle / 2 < Math.abs(terrainAngle)) {
        correctedAngle = 0;
    }
    else {
        correctedAngle = fullRotations + 0 < Math.sign(terrainAngle) ?
            Math.min(terrainAngle, maxLandingAngle * Math.sign(terrainAngle)) :
            Math.max(terrainAngle, maxLandingAngle * Math.sign(terrainAngle));
    }
    let commands: Command[] = [];
    let tick = status.tick + 1;
    if (status.touchdown && Math.abs(status.angle) < maxLandingAngle) {
        commands = [
            { engine: 'off' },
            { rotation: 'off' }
        ];
    } else {
        if (Math.abs(status.velocity.y) < 0.1 && maxLandingAngle < Math.abs(status.angle)) {
            // looks like the lander might be stuck lying down.
            // in this case we just need to continually rotate until we are upright

            console.warn(`I'm bloody stuck!`);
            // work out which way to boost
            const halfRotations = Math.floor(status.angle / Math.PI);
            const invert = halfRotations % 2 === 0 ? 1 : -1;
            const direction = status.angle * invert;
            const rotation: RotationState = 0 < direction ? 'ccw' : 'cw';
            commands = [{ rotation }];
        } else if (0.05 < Math.abs(status.angle - correctedAngle)) {
            console.log(`current angle: ${status.angle}, terrain angle: ${terrainAngle}, rotating to ${correctedAngle}`);
            let rotate = rotateTo(status, correctedAngle);
            commands = rotate.commands;
            tick = round(rotate.tick);
        }
    }
    return {
        goToNextStage: false,
        commands,
        tick
    };
}

/**
 * Given a position of the lander, return the terrain map points immediately surrounding that position
 */
function getSurroundingPoints(pos: Point, terrain: Point[]): [Point, Point] {
    // get the constant x distance between each point
    const separation = terrain[1].x;
    const prevIndex = Math.floor(pos.x / separation);
    return [terrain[prevIndex], terrain[prevIndex + 1]];
}


/**
 * Returns the land elevation (i.e. the y coordinate) at the given x position on the map
 */
function getElevationAt(x: number, map: TerrainMap): number {
    let bestMatch: {
        point: Point,
        proximity: number
    } | undefined;
    for(let i = 0; i < map.terrain.length - 1; i++) {
        const point = map.terrain[i];
        const proximity = Math.abs(point.x - x);
        if (!bestMatch || proximity < bestMatch.proximity) {
            bestMatch =  { point, proximity };
        }
    }
    return bestMatch!.point.y;
}

/**
 * Stopping distance formula: https://physics.stackexchange.com/a/3821
 * d = v^2 / 2a
 */
function calculateStoppingDistance(velocity: number, acceleration: number): number {
    return (velocity ** 2) / (2 * acceleration);
}

/**
 * Calculates the number of ticks required to move through a distance given an initial velocity
 * and an acceleration.
 * d = vt + 1/2 at^2, or
 * t = (-v+√(v^2+2ad))/a
 * @param initV Initial velocity on the axis of motion
 * @param a Constant acceleration being applied on the axis of motion
 * @param distance Distance to calculate time for
 */
function calculateTimeToReachDistance(initV: number, a: number, distance: number): number {
    const v = initV;
    return (-v + Math.sqrt(v**2 + 2 * a * Math.abs(distance))) / a;
}

/**
 * Calculate the time needed to bring the x velocity to 0
 */
function calculateTimeToStopX(status: Status): number {
    const initV = status.velocity;
    const initAngle = status.angle;
    const a = Math.abs((Math.sin(initAngle) * FULL_THRUST));
    const v = initV.x;

    return Math.abs(v / a);
}

/**
 * Given the current status of the lander, produces an array of commands which will orient it to the given angle.
 */
function rotateTo(status: Status, angleInRadians: number): CalculationResult {
    // first stop any rotation
    const initial = stopRotation(status);

    const initialAngle = initial.restAngle;
    const fullRotations = Math.floor(Math.abs(initialAngle / FULL_ROTATION)) * Math.sign(initialAngle);
    const finalAngle = fullRotations * FULL_ROTATION + angleInRadians;
    const delta = finalAngle - initialAngle;
    // first we calculate the number of ticks required to accelerate the lander to half way
    // to the target angle, using s = 1/2 at^2, or
    // 2s = at^2
    // t^2 = 2s/a
    // t = 2√(2s/a)
    const halfWay = Math.abs(delta / 2);
    const ticksToHalfWay = Math.sqrt(2 * halfWay / ROTATION_ACCELERATION);
    const direction1: RotationState = 0 < delta ? 'ccw' : 'cw';
    const direction2: RotationState = 0 < delta ? 'cw' : 'ccw';
    const t1 = initial.tick + 1;
    const t2 = t1 + round(ticksToHalfWay);
    const t3 = t2 + round(ticksToHalfWay);
    const commands = initial.commands.concat([
        { rotation: direction1, tick: t1 },
        { rotation: direction2, tick: t2 },
        { rotation: 'off', tick: t3 }
    ]);

    return { tick: t3 + 1, commands };
}

/**
 * Creates a CommandSet which will reduce the lander's rotationSpeed to zero.
 */
function stopRotation(status: Status): { restAngle: number; tick: number; commands: Command[]; } {
    let commands: Command[] = [];
    let tick = status.tick;
    let restAngle = status.angle;
    if (status.rotationSpeed !== 0) {
        const ticksToRest = Math.abs(status.rotationSpeed) / ROTATION_ACCELERATION;
        tick += round(ticksToRest);
        const direction: RotationState = 0 < status.rotationSpeed ? 'cw' : 'ccw';
        const acceleration = direction === 'cw' ? -ROTATION_ACCELERATION : ROTATION_ACCELERATION;
        restAngle = status.angle + status.rotationSpeed * ticksToRest + (acceleration * ticksToRest**2) / 2;
        commands = [
            { rotation: direction },
            { rotation: 'off', tick: tick }
        ];
    }
    return { restAngle, tick, commands };
}

/**
 * The server does not like decimal ticks, so we often need to round
 */
function round(val: number): number {
    return Math.round(val);
}