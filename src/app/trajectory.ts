import {Point, Status} from './models';

const GRAVITY = 0.01;

export function trajectory(status: Status): Point[] {
    let { x, y } = status.position;
    let vy = status.velocity.y;
    const path: Point[] = [];
    while(0 < y) {
        y += vy;
        x += status.velocity.x;
        path.push({ x, y });
        vy -= GRAVITY;
    }
    return path;
}
